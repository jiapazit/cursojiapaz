from behave import given, when, then
from calculadora import Calculadora


@given(u'que ingreso los números "{num1}" y "{num2}"')
def step_impl(context, num1, num2):
    context.calc = Calculadora()
    context.num1 = int(num1)
    context.num2 = int(num2)

@given(u'que ingreso el caracter "{caracter}" y el número "{num2}"')
def step_impl(context, caracter, num2):
    context.calc = Calculadora()
    context.num1 = caracter
    context.num2 = int(num2)
    
@given(u'que ingreso los números decimales "{num1}" y "{num2}"')
def step_impl(context, num1, num2):
    context.calc = Calculadora()
    context.num1 = float(num1)
    context.num2 = float(num2)

    
@when(u'realizo el cálculo')
def step_impl(context):
    context.resultado = context.calc.sumar(context.num1, context.num2)


@then(u'puedo ver el resultado "{esperado}"')
def step_impl(context, esperado):
    assert int(esperado) == context.resultado
    
@then(u'puedo ver el mensaje "{mensaje}"')
def step_impl(context, mensaje):
    assert mensaje == context.resultado
    
@then(u'puedo ver el resultado en decimal "{esperado}"')
def step_impl(context, esperado):
    assert float(esperado) == context.resultado


    