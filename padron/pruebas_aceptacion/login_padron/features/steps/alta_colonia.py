from behave import given, when, then
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

@given(u'presiono el botón de Iniciar sesión')
def step_impl(context):
    xpath = '/html/body/main/section/div/div/div/div[1]/div/div[2]/form/div[4]/button'
    context.driver.find_element(By.XPATH, xpath).click()

@given(u'me voy a dirección "{url}"')
def step_impl(context, url):
    context.driver.get(url)
    
@given(u'doy clic en el link "{link}"')
def step_impl(context, link):
    context.driver.find_element(By.LINK_TEXT, link).click()

@given(u'nuevamente doy click en el botón "{link}"')
def step_impl(context, link):
    time.sleep(0.5)
    context.driver.find_element(By.LINK_TEXT, link).click()

@given(u'tecleo en Nombre "{nombre}" y en Localidad "{localidad}"')
def step_impl(context, nombre, localidad):
    context.driver.find_element(By.NAME, 'nombre').send_keys(nombre)
    context.driver.find_element(By.NAME, 'localidad').send_keys(localidad)


@when(u'presiono el botón "GUARDAR"')
def step_impl(context):
    context.driver.find_element(By.NAME, '_save').click()


@then(u'puedo el mensaje "{colonia}"')
def step_impl(context, colonia):
    resultado = context.driver.find_element(By.CLASS_NAME, 'success').text
    context.test.assertIn(colonia, resultado)