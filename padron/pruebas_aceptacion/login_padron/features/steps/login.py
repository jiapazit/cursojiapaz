from behave import given, when, then
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time


@given(u'que ingreso a la dirección "{url}"')
def step_impl(context, url):
    
    context.driver.get(url)


@given(u'que tecleo el usuario "{usuario}" y el password "{contra}"')
def step_impl(context, usuario, contra):
    context.driver.find_element(By.NAME, 'username').send_keys(usuario)
    context.driver.find_element(By.NAME, 'password').send_keys(contra)
    

@when(u'presiono el botón de Iniciar sesión')
def step_impl(context):
    xpath = '/html/body/main/section/div/div/div/div[1]/div/div[2]/form/div[4]/button'
    # time.sleep(5)
    context.driver.find_element(By.XPATH, xpath).click()


@then(u'puedo ver el mensaje "{mensaje}"')
def step_impl(context, mensaje):
    resultado = context.driver.find_element(By.TAG_NAME, 'h3').text
    context.test.assertEquals(resultado, mensaje)
    
@then(u'puedo ver el mensaje de error "{mensaje}"')
def step_impl(context, mensaje):
    resultado = context.driver.find_element(By.CLASS_NAME, 'alert').text
    context.test.assertEquals(resultado, mensaje)