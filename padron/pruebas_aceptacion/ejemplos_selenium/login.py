from threading import local
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

options = webdriver.ChromeOptions()
# options.add_argument("no-sandbox")
# options.add_argument("--disable-gpu")
options.add_argument("--window-size=1600,900")
# options.add_argument("--window-size=600,800")
        
driver = webdriver.Chrome(options=options)



def login(username, password):
    driver.get('http://localhost:8000/usuarios/login')

    driver.find_element(By.NAME, 'username').send_keys(username)
    driver.find_element(By.NAME, 'password').send_keys(password)
    xpath = '/html/body/main/section/div/div/div/div[1]/div/div[2]/form/div[4]/button'
    driver.find_element(By.XPATH, xpath).click()
    # Busca el botón d
    # driver.find_element(By.ID, 'iconNavbarSidenav').click()
    # time.sleep(0.5)
def nueva_localidad(nombre, descripcion):
    driver.find_element(By.LINK_TEXT, 'Localidades').click()

    driver.find_element(By.LINK_TEXT, 'NUEVA').click()

    driver.find_element(By.NAME, 'nombre').send_keys(nombre)
    driver.find_element(By.NAME, 'descripcion').send_keys(descripcion)
    driver.find_element(By.XPATH, '/html/body/main/div/div/div[2]/input').click()

def buscar_localidad(nombre, boton):
    localidad = None
    while True:
        try:
            siguiente = driver.find_element(By.LINK_TEXT, 'Siguiente')
        except:
            siguiente = None
        
        tbody = driver.find_element(By.TAG_NAME, 'tbody')
        
        for tr in tbody.find_elements(By.TAG_NAME, 'tr'):
            tds = tr.find_elements(By.TAG_NAME, 'td')
            if tds[0].text == nombre:
                localidad = tds[2].find_element(By.LINK_TEXT, boton)
                break
        if localidad:
            return localidad
        else:
            if not siguiente:
                return None
        

def lista_localidades():
    driver.find_element(By.LINK_TEXT, 'Localidades').click()
    
    tbody = driver.find_element(By.TAG_NAME, 'tbody')

    for tr in tbody.find_elements(By.TAG_NAME, 'tr'):
        tds = tr.find_elements(By.TAG_NAME, 'td')
        print ('Nombre: '+tds[0].text)
        print ('Descripción: '+tds[1].text)

def editar_localidad(nombre, descripcion):
    localidad = buscar_localidad(nombre, 'EDITAR')
    if localidad:
        localidad.click()
        nombre_el = driver.find_element(By.NAME, 'nombre')
        nombre_el.clear()
        nombre_el.send_keys(nombre)
        descripcion_el = driver.find_element(By.NAME, 'descripcion')
        descripcion_el.clear()
        descripcion_el.send_keys(descripcion)
        driver.find_element(By.XPATH, '/html/body/main/div/div/div[2]/input').click()
    else:
        print('No se encontró la localidad '+nombre)  

def eliminar_localidad(nombre):
    localidad = buscar_localidad(nombre, 'ELIMINAR')
    if localidad:
        localidad.click()
        driver.find_element(By.XPATH, '/html/body/main/div/form/input[2]').click()
    
    
def nuevo_usuario(datos):
    driver.find_element(By.LINK_TEXT, 'Usuarios').click()

    driver.find_element(By.LINK_TEXT, 'NUEVO').click()

    for dato in datos:
        # print(dato)
        # print(datos[dato])
        driver.find_element(By.NAME, dato).send_keys(datos[dato])
        html = driver.find_element(By.TAG_NAME, 'html')
        html.send_keys(Keys.ARROW_DOWN)
        html.send_keys(Keys.ARROW_DOWN)
        html.send_keys(Keys.ARROW_DOWN)
        # html.send_keys(Keys.ARROW_DOWN)
        #    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);") 
        
    
    # driver.find_element(By.NAME, 'usuario-genero').send_keys(Keys.RETURN)
    driver.find_element(By.XPATH, '/html/body/main/div/div/div[2]/input').click()
 
login('alex', 'alexmau1')
datos = {
    'user-username': 332233,
    'user-email': '33333@hotcakes.com',
    'usuario-derivada': '3',
    'usuario-inmueble': '20',
    'usuario-toma': '50',
    'usuario-nombre': 'Pedro',
    'usuario-apellido_paterno': 'Mares',
    'usuario-apellido_materno': 'López',
    'usuario-curp': 'MAGA801121HZSRNL07',
    'usuario-localidad': 'Apulco 2',
    'usuario-colonia': 'Las palmas',
    'usuario-estado_civil': 'Soltero(a)',
    'usuario-genero': 'Masculino',
    'usuario-foto': '/Users/alexmau/Downloads/img1.jpeg',
    'usuario-archivo': '/Users/alexmau/Downloads/EstatutoGeneralUAZ.pdf',
}
nuevo_usuario(datos)
# lista_localidades()
# nueva_localidad('Otra', 'masss')
# editar_localidad('Apulco 2', 'Nueva descripción')
# eliminar_localidad('Calera')
