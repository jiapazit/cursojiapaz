>>> from calculadora import Calculadora
>>> calc = Calculadora()

>>> calc.sumar(3, 4)
7

>>> calc.sumar(4, 4)
8

>>> calc.sumar(-4, 4)
0

>>> calc.sumar(0, 4)
4

>>> calc.sumar('x', 4)
'Solo se admiten numeros'

>>> calc.sumar(4,'x')
'Solo se admiten numeros'

>>> calc.sumar(5.3, 9.2)
14.5

>>> calc.sumar(761523612536712536, 9817236512312635761657252)
9817237273836248298369788

>>> calc.sumar(5.3, 9.2, 7)
'Solo se aceptan 2 parametros'

>>> calc.restar(9, 8)
1
