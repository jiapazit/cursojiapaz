from rest_framework import serializers
from usuarios.models import Localidad


class LocalidadSelerizer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    nombre = serializers.CharField(max_length=200)
    
    def create(self, validated_data):
        return Localidad.objects.create(**validated_data)
        # return Localidad.objects.create(
        #     nombre = validated_data['nombre'],
        #     descripcion = validated_data['descripcion'],
        # )

    def update(self, instance, validated_data):
        instance.nombre = validated_data.get('nombre', instance.nombre)
        instance.descripcion = validated_data.get('descripcion', instance.descripcion)
        instance.save()
    
class LocalidadSerializerModel(serializers.ModelSerializer):
    class Meta:
        model = Localidad
        # fields = '__all__'
        fields = ['id', 'nombre', 'descripcion']