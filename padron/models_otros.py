# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class CertiNormalesAlumnocertificado(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hist_estudio = models.IntegerField()
    curp = models.CharField(max_length=18)
    nombre = models.CharField(max_length=35)
    primer_apellido = models.CharField(max_length=35)
    segundo_apellido = models.CharField(max_length=35, blank=True, null=True)
    estatus = models.CharField(max_length=1)
    fecha_firma = models.DateTimeField()
    fecha_cert_texto = models.CharField(max_length=100, blank=True, null=True)
    fecha_certificacion = models.DateField()
    sello_sep = models.CharField(max_length=3000, blank=True, null=True)
    fecha_sello_sep = models.DateTimeField(blank=True, null=True)
    folio_sep = models.CharField(max_length=60, blank=True, null=True)
    promedio = models.CharField(max_length=3)
    promedio_texto = models.CharField(max_length=50, blank=True, null=True)
    tipo_cert = models.CharField(max_length=1)
    sello_seduzac = models.CharField(max_length=3000, blank=True, null=True)
    formato = models.CharField(max_length=4, blank=True, null=True)
    carrera = models.ForeignKey('CertiNormalesCarrera', models.DO_NOTHING)
    escuela = models.ForeignKey('CertiNormalesEscuela', models.DO_NOTHING)
    tipo_certificacion = models.CharField(max_length=1, blank=True, null=True)
    numero_lote = models.CharField(max_length=50, blank=True, null=True)
    periodo1 = models.CharField(max_length=10)
    periodo10 = models.CharField(max_length=10)
    periodo2 = models.CharField(max_length=10)
    periodo3 = models.CharField(max_length=10)
    periodo5 = models.CharField(max_length=10)
    periodo6 = models.CharField(max_length=10)
    periodo7 = models.CharField(max_length=10)
    periodo8 = models.CharField(max_length=10)
    periodo9 = models.CharField(max_length=10)
    periodo11 = models.CharField(max_length=10)
    periodo12 = models.CharField(max_length=10)
    semestre = models.CharField(max_length=2)
    genero = models.CharField(max_length=1)
    periodo4 = models.CharField(max_length=10)
    motivo_cancela = models.CharField(max_length=2, blank=True, null=True)
    autoridad = models.ForeignKey('CertiNormalesAutoridadeducativa', models.RESTRICT)
    folio_control = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        
        db_table = 'certi_normales_alumnocertificado'


class CertiNormalesAutoridadeducativa(models.Model):
    id = models.BigAutoField(primary_key=True)
    curp = models.CharField(max_length=25)
    nombre = models.CharField(max_length=80)
    primer_apellido = models.CharField(max_length=80)
    segundo_apellido = models.CharField(max_length=80)
    id_cargo = models.CharField(max_length=20)
    cargo = models.CharField(max_length=100)
    no_serie_certificado = models.CharField(max_length=100)
    certificado = models.CharField(max_length=3000)
    estatus = models.CharField(max_length=1)

    class Meta:
        
        db_table = 'certi_normales_autoridadeducativa'


class CertiNormalesCalificacion(models.Model):
    id = models.BigAutoField(primary_key=True)
    semestre = models.SmallIntegerField()
    calificacion = models.DecimalField(max_digits=3, decimal_places=1)
    estatus = models.CharField(max_length=1, blank=True, null=True)
    alumno = models.ForeignKey(CertiNormalesAlumnocertificado, models.DO_NOTHING)
    materia = models.ForeignKey('CertiNormalesMateria', models.DO_NOTHING)

    class Meta:
        
        db_table = 'certi_normales_calificacion'


class CertiNormalesCarrera(models.Model):
    id = models.BigAutoField(primary_key=True)
    clave_dgp = models.CharField(max_length=10)
    nombre = models.CharField(max_length=120)
    plan_estudios = models.CharField(max_length=5)
    es_reestructurado = models.CharField(max_length=2)
    modalidad = models.ForeignKey('CertiNormalesModalidad', models.DO_NOTHING)
    tipo = models.CharField(max_length=3)

    class Meta:
        
        db_table = 'certi_normales_carrera'


class CertiNormalesCurricula(models.Model):
    id = models.BigAutoField(primary_key=True)
    creditos = models.DecimalField(max_digits=5, decimal_places=2)
    posicion = models.IntegerField()
    semestre = models.SmallIntegerField()
    carrera = models.ForeignKey(CertiNormalesCarrera, models.DO_NOTHING)
    materia = models.ForeignKey('CertiNormalesMateria', models.DO_NOTHING)

    class Meta:
        
        db_table = 'certi_normales_curricula'


class CertiNormalesEscuela(models.Model):
    cct = models.CharField(primary_key=True, max_length=10)
    clave_dgp = models.CharField(max_length=10)
    nombre = models.CharField(max_length=80)
    id_municipio = models.CharField(max_length=3)
    acuerdo = models.CharField(max_length=20, blank=True, null=True)
    fecha_acuerdo = models.DateField(blank=True, null=True)
    id_genero = models.CharField(max_length=1)
    estatus = models.CharField(max_length=1)

    class Meta:
        
        db_table = 'certi_normales_escuela'


class CertiNormalesMateria(models.Model):
    id = models.BigAutoField(primary_key=True)
    nombre = models.CharField(max_length=150)
    tipo = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        
        db_table = 'certi_normales_materia'


class CertiNormalesModalidad(models.Model):
    id = models.BigAutoField(primary_key=True)
    modalidad = models.CharField(max_length=50)

    class Meta:
        
        db_table = 'certi_normales_modalidad'


class CertiNormalesMunicipio(models.Model):
    id = models.BigAutoField(primary_key=True)
    nombre = models.CharField(max_length=100)

    class Meta:
        
        db_table = 'certi_normales_municipio'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        
        db_table = 'django_session'
