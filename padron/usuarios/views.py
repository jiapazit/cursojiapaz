from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from usuarios.models import Usuario
from usuarios.forms import FormUsuario, FormBusqueda, FormUser
from django.contrib import messages
from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.auth.views import LoginView
from django.contrib.auth.forms import AuthenticationForm
from django.views.generic import TemplateView
from django.contrib.auth.models import Group, User


class EntrarView(LoginView):
    template_name = 'login.html'
    form_class = AuthenticationForm

class PaginaPrincipal(TemplateView):
    template_name = 'principal.html'

@login_required()
def lista_usuarios(request):
    # usuarios = Usuario.objects.all()
    # select * from usuario;
    
    # usuarios = Usuario.objects.all().values('nombre','id')
    grupos = Group.objects.all()
    usuarios = Usuario.objects.all().order_by('nombre','apellido_materno')
    form = FormBusqueda()
    if request.method == 'POST':
        form = FormBusqueda(request.POST)
        nombre = request.POST.get('nombre', None)
        apellido_paterno = request.POST.get('apellido_paterno', None)
        apellido_materno = request.POST.get('apellido_materno', None)
        genero = request.POST.get('genero', None)
        estado_civil = request.POST.get('estado_civil', None)
        localidad = request.POST.get('localidad', None)
        if nombre:
            usuarios = usuarios.filter(nombre=nombre)
        if apellido_paterno:
            usuarios = usuarios.filter(apellido_paterno=apellido_paterno)
        if apellido_materno:
            usuarios = usuarios.filter(apellido_materno=apellido_materno)
        if genero:
            usuarios = usuarios.filter(genero=genero)
        if estado_civil:
            usuarios = usuarios.filter(estado_civil=estado_civil)
        if localidad:
            usuarios = usuarios.filter(localidad__nombre__contains=localidad)
    
    paginator = Paginator(usuarios, 10)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
        
    print(page_obj)
    context = {
        # 'usuarios': Usuario.objects.filter(nombre__exact='jUan') #case-sensitive
        # 'usuarios': Usuario.objects.filter(nombre__iexact='jUan') #no case-sensitive
        # 'usuarios': Usuario.objects.filter(nombre__startswith='J') #no case-sensitive
        # 'usuarios': Usuario.objects.filter(nombre__endswith='n') #no case-sensitive
        # 'usuarios': Usuario.objects.filter(nombre__contains='a') #no case-sensitive
        # 'usuarios': Usuario.objects.filter(nombre__contains='a', apellido_paterno__contains='au'), #no case-sensitive
        'usuarios': page_obj,
        'form': form,
        'grupos': grupos
    }
    
    return render(request, 'usuarios.html', context)

@permission_required('usuarios.add_usuario')
def nuevo_usuario(request):
    if request.method == 'POST':
        form = FormUsuario(request.POST, prefix='usuario', files=request.FILES)
        form_user = FormUser(request.POST, prefix='user')
        if form_user.is_valid() and form.is_valid():
            user = form_user.save(commit=False)
            user.set_password('Temporal123') 
            user.save()
            
            usuario = form.save(commit=False)
            usuario.user = user
            usuario.save()
            
            messages.success(request, f"El usuario {usuario.nombre} se guardo con éxito")
            return redirect('lista_usuarios')
        else:
            messages.error(request, 'Ocurrió un error al guardar el usuario; revisa tu información')
            
    else:
        form = FormUsuario(prefix='usuario')
        form_user = FormUser(prefix='user')
    
    context = {
        'form': form,
        'form_user': form_user
    }
    
    return render(request, 'nuevo_usuario.html', context)

@permission_required('usuarios.todo_usuarios')
def editar_usuario(request, id):
    usuario = Usuario.objects.get(id=id)
    if request.method == 'POST':
        form = FormUsuario(request.POST, instance=usuario, files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, f"El usuario {usuario.nombre} se guardó con éxito")
            return redirect('lista_usuarios')
        else:
            messages.error(request, 'Ocurrió un error al guardar el usuario; revisa tu información')
            
    else:
        form = FormUsuario(instance=usuario)
        
    return render(request, 'nuevo_usuario.html', {'form':form, 'editar':True}) 


def eliminar_usuario(request, id):
    try:
        Usuario.objects.get(id=id).delete()
        messages.success(request, f"El usuario se eliminó con éxito")
    except:
        messages.error(request, 'Ocurrió un error al eliminar el usuario')
      
    return redirect('lista_usuarios')
    
def ver_usuario(request, id):
    usuario = Usuario.objects.get(id=id)
    return render(request, 'detalle_usuario.html', {'usuario':usuario})

def asigna_grupos(request):
    # print (request.POST)
    try:
        id_grupo = int(request.POST.get('grupos',0))
        grupo = Group.objects.get(id=id_grupo)
        for item in request.POST:
            if request.POST[item] == 'on':
                usuario = User.objects.get(id=int(item))
                usuario.groups.add(grupo)
        messages.success(request, f"Se asignaron los usuarios al grupo {grupo.name}")
    except:
        messages.error(request, "Ocurrió un error al asignar los usuarios al grupo")
    return redirect('lista_usuarios')