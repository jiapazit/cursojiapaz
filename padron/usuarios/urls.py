from django.urls import path
from usuarios import views
from django.contrib.auth.views import LogoutView


urlpatterns = [
    path('', views.lista_usuarios, name='lista_usuarios'),
    path('nuevo', views.nuevo_usuario, name='nuevo_usuario'),
    path('eliminar/<int:id>', views.eliminar_usuario, name='eliminar_usuario'),
    path('editar/<int:id>', views.editar_usuario, name='editar_usuario'),
    path('datalle/<int:id>', views.ver_usuario, name='ver_usuario'),
    
    path('login', views.EntrarView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    
    path('asigna-grupo', views.asigna_grupos, name='asigna_grupos'),
    
]


# jiapaz.gob.mx/usuarios
# jiapaz.gob.mx/usuarios/nuevo
# jiapaz.gob.mx/usuarios/eliminar/1
# jiapaz.gob.mx/usuarios/
# jiapaz.gob.mx/hola