from django.test import TestCase
from django.contrib.auth.models import User


class UsuariosViews(TestCase):

    def test_lista_usuarios(self):
        user = User.objects.create(username='alex', password='admin123')
        # self.client.login(username='alex', password='admin123')
        self.client.force_login(user)
        
        response = self.client.get('/usuarios/')
        print(response.content)
        self.assertEqual(response.status_code, 200)