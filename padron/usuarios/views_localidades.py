from django.http import JsonResponse
from django.urls import reverse_lazy
from django.core.paginator import Paginator
from usuarios.models import Localidad, Colonia
from usuarios.forms import FormLocalidad
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

class ListaLocalidades(ListView):
    model = Localidad
    paginate_by = 2
    # queryset = Localidad.objects.filter(nombre__startwith='Ap')
    # select * from localidad where nombre like 'Ap%'
    # template_name = 'lista_localidades.html'
    # context_object_name = 'localidades'
    
class NuevaLocalidad(CreateView):
    model = Localidad
    # fields = '__all__'
    success_url = reverse_lazy('localidades:lista')
    form_class = FormLocalidad
    
    # def form_valid(self, form):
    #     self.object = form.save()
    #     return super().form_valid(form)
    
class EliminarLocalidad(DeleteView):
    model = Localidad
    success_url = reverse_lazy('localidades:lista')
    
class EditarLocalidad(UpdateView):
    model = Localidad
    form_class = FormLocalidad
    success_url = reverse_lazy('localidades:lista')
    extra_context = {'editar': True}
    
class DetalleLocalidad(DetailView):
    model = Localidad
    
    
def busca_colonias(request):
    id = request.POST.get('id', None)
    if id:
        colonias = Colonia.objects.filter(localidad__id=id)
        data = [{'id':colonia.id,'nombre':colonia.nombre} for colonia in colonias]
        
        return JsonResponse(data=data, safe=False)
    return JsonResponse(data={'error':'Falta ID'}, safe=False)
