from django.shortcuts import render, redirect
from inmuebles.forms import FormInmueble
from .models import Inmueble

def lista_inmuebles(request):
    inmuebles = Inmueble.objects.all()
    return render(request, 'lista_inmuebles.html',{'inmuebles':inmuebles})
    

def nuevo_inmueble(request):
    if request.method == 'POST':
        form = FormInmueble(request.POST)
        if form.is_valid():
            form.save()
            return redirect('inmuebles:lista')
            
    else:
        form = FormInmueble()
    return render(request, 'nuevo_inmueble.html',{'form':form})