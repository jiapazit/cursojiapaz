from django.db import models

class Inmueble(models.Model):
    calle = models.CharField(max_length=250)
    numero_exterior = models.CharField(max_length=250)
    usuario = models.ForeignKey("usuarios.Usuario", related_name='inmueble_usuario', verbose_name="Usuario", on_delete=models.DO_NOTHING)
    latitud = models.CharField(max_length=50)
    longitud = models.CharField(max_length=50)
    
    
    def __str__(self):
        return self.calle