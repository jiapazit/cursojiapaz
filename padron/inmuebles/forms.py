from django import forms
from inmuebles.models import Inmueble


class FormInmueble(forms.ModelForm):
    class Meta:
        model = Inmueble
        fields = '__all__'
        
        widgets = {
            'calle':forms.TextInput(attrs={'class':'form-control'}),
            'numero_exterior':forms.NumberInput(attrs={'class':'form-control'}),
            'latitud':forms.NumberInput(attrs={'class':'form-control','readonly':'true'}),
            'longitud':forms.TextInput(attrs={'class':'form-control','readonly':'true'}),
            'usuario': forms.Select(attrs={'class':'form-select'}),
        }