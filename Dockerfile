FROM debian:bullseye-slim

RUN apt-get update
RUN apt-get install apache2 python3 python3-pip libmariadb-dev \
    libapache2-mod-wsgi-py3 python3-dev openssh-client build-essential libssl-dev libffi-dev  -y

# Configure timezone
ENV TZ=America/Mexico_City
RUN ln -snf  /etc/l/usr/share/zoneinfo/$TZocaltime && echo $TZ > /etc/timezone

WORKDIR /app

COPY ./padron/requirements.txt /app/
# COPY ./seguimiento.conf /etc/apache2/sites-available/

# RUN adduser --disabled-password user

# RUN mkdir /app/media
# RUN chown www-data:www-data /app/media/ -R

# RUN a2ensite seguimiento.conf


RUN pip3 install --upgrade pip
RUN pip3 install -r /app/requirements.txt

EXPOSE 80

CMD [ "apachectl", "-DFOREGROUND" ]



